This is a simple [Fabric](http://fabfile.org/) -based script to back up
the volatile parts of a Django web application: remote media files and
PostgreSQL database.
