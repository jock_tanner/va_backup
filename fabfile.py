#!/usr/bin/env python

import sys

from invoke import Responder
from fabric import Connection, task

try:
    from settings import *
except ImportError:
    print('Create settings file using the template.')
    sys.exit(1)


@task
def va_backup(conn):
    # create backup directory
    conn.local(f"mkdir -p '{BACKUP_DIR}'", warn=True)

    # transfer database in SQL format
    with open(SQL_FILE, 'w') as sql_file:
        conn.sudo(
            f"pg_dump '{DB_NAME}'",
            user='postgres', password=PASSWORD,
            out_stream=sql_file, hide='out',
        )

    # transfer remote directories
    rdb = os.path.join(os.environ['VIRTUAL_ENV'], 'bin', 'rdiff-backup')
    excludes = ' '.join([f"--exclude '{e}'" for e in EXCLUDE_DIRS])
    conn.local(
        f"{rdb} {excludes} '{USER}@{HOST}::{REMOTE_MEDIA_DIR}' '{MEDIA_DIR}'",
        pty=True,
        watchers=[Responder(pattern=r'password:', response=f'{PASSWORD}\n')],
    )
    return 0


if __name__ == '__main__':
    conn = Connection(HOST, user=USER, connect_kwargs={'password': PASSWORD})
    sys.exit(va_backup(conn))
